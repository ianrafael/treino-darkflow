from decouple import config
from src.Controller.FileController import FileController
from webserver import WebServer
from src.Controller.Train import Train

options = {
        "model": config('DARKFLOW_MODEL'),
        "load": config('DARKFLOW_LOAD'),
        "batch": 8,
        "epoch": 10,
        "gpu": 0.75,
        "train": True,
        "annotation": config('DATASET_FOLDER')+'annotations/',
        "dataset": config('DATASET_FOLDER')+'images/'
    }
train = Train(options)
web_server = WebServer(train)
web_server.main()

while(True):
    pass