import zipfile
from decouple import config
import requests
import cv2
import flask
import base64
import numpy as np 
import os
import re
from shutil import copyfile
from flask import jsonify
import json

BASE_API_TRAIN = config('BASE_API_TRAIN')
NRORG = config('NRORG')
IMAGE_PATH = config('IMAGE_PATH')
ANNOTATION_PATH = config('ANNOTATION_PATH')
FILE_SERVER_TOKEN = config('FILE_SERVER_TOKEN')
DATASET_FOLDER = config('DATASET_FOLDER')
DARKFLOW_MODEL = config('DARKFLOW_MODEL')
TRAINNINGDATA_PATH = config('TRAINNINGDATA_PATH')

class FileController:
    def __init__(self, train):
        self.train = train
        self.dataUrl = list()
        self.class_num = int()
        self.config_filename = str()
        self.folders_list = [IMAGE_PATH, ANNOTATION_PATH, DATASET_FOLDER+'images', DATASET_FOLDER + 'annotations', 'TranningData/', 'ckpt/']
        for folder_path in self.folders_list:
            if not os.path.exists(folder_path):
                    os.makedirs(folder_path)
        self.removeOldFiles('ckpt/')
        pass
    
    def removeOldFiles(self, path):        
        files = os.listdir(path)
        for filename in files:
            os.remove(path+filename)

    def getDatasetUrl(self):
        self.dataUrl = list()
        params = {
            'NRORG':NRORG,
            'in':'NRORG'
        }
        r = requests.get(BASE_API_TRAIN+'image', data=params)
        data = r.json()
        data = data['data']
        for row in data:
            if row['ITEMANOTURL']!=None:
                urls = {
                    'annotationUrl' : row['ITEMANOTURL'],                    
                    'imageUrl' : row['ITEMIMAGEMURL'],
                    'cod_image': row['CDIMAGEMITEM']
                }
                self.dataUrl.append(urls)                
        pass

    def downloadFiles(self):
        self.removeOldFiles(ANNOTATION_PATH)
        self.removeOldFiles(IMAGE_PATH)
        count = 0
        for url in self.dataUrl:            
            self.train.setStatus(str(count) + ' de ' + str(len(self.dataUrl)))
            filename = str(url['cod_image'])
            count += 1            
            image_base64 = self.get_file_contents(url['imageUrl'])
            decoded_data = base64.b64decode(image_base64)
            np_data = np.fromstring(decoded_data, np.uint8)
            image = cv2.imdecode(np_data, cv2.IMREAD_UNCHANGED)
            cv2.imwrite(IMAGE_PATH + filename + '.jpg', image)

            # Download Annotation            
            annotation_base64 = self.get_file_contents(url['annotationUrl'])
            data_bytes = base64.b64decode(annotation_base64)
            f = open(ANNOTATION_PATH + filename + '.xml', 'wb')
            f.write(data_bytes)
            f.close()
        pass
    
    def get_file_contents(self, url):            
            paramters = {
                    'row':{
                        'url':url
                    }
                }
            file = requests.post(BASE_API_TRAIN + 'getfileb64', json = paramters)
            file = file.json()
            file_base64 = file['dataset']['response']['base64file']
            return file_base64


    def editAnnotation(self):
        files_name = os.listdir(ANNOTATION_PATH)
        count = 0
        for file_name in files_name:
            count += 1
            f=open('./storage/annotations/' + file_name, 'r+')
            string_file = f.read()
            f.close()
            string_file = string_file.replace('FOLDER__NAME', 'images')            
            string_file = string_file.replace('FILE__NAME', file_name.replace('xml', 'jpg'))
            string_file = string_file.replace('FULL__PATH', os.path.abspath(DATASET_FOLDER) + '\\images\\' + file_name[0:-4] + '.jpg')

            f=open('./storage/annotations/' + file_name, 'r+')
            f.write(string_file)
            f.close()

            self.train.setStatus(str(count) + ' de ' + str(len(files_name)))     


    def prepareDataSet(self):        
        self.removeOldFiles(DATASET_FOLDER + 'annotations/')
        self.removeOldFiles(DATASET_FOLDER + 'images/')
        files_name = os.listdir(IMAGE_PATH)
        count = 0
        for file_name in files_name:
            count += 1
            image_path = IMAGE_PATH + file_name
            destination_path = os.path.abspath(DATASET_FOLDER + 'images/' + file_name)
            copyfile(image_path, destination_path)
            
            annotation_path = ANNOTATION_PATH + file_name[0:-3] + 'xml'
            destination_path = os.path.abspath(DATASET_FOLDER+'annotations/' + file_name[0:-3] + 'xml')
            copyfile(annotation_path, destination_path)
            self.train.setStatus(str(count) + ' de ' + str(len(files_name)))
        pass

    def generateLabelsFile(self):
        self.train.setStatus('listando labels')
        annotation_path = ANNOTATION_PATH 
        class_list = list()
        have = 0
        for file in os.listdir(annotation_path):
            ann = open(annotation_path + file, 'r')
            for line in ann:
                if line.find('<name>')!= -1:
                    current_class = re.sub('\s+', '', line)
                    current_class = current_class.replace(' ', '')
                    current_class = current_class.replace('</name>', '')
                    current_class = current_class.replace('<name>', '')
                    have=0
                    for itens in class_list:
                        if itens  == current_class + '\n':
                            have = 1
                    if have == 0:
                        class_list.append(current_class + '\n')      
            ann.close()

        self.class_num = len(class_list)
        labels = open('labels.txt', 'w')
        labels.writelines(class_list)
        labels.close()
    
    def generateCfgFile(self):
        self.train.setStatus('Gerando configurações')
        cfg_file = open('./cfg/tiny-yolo-voc__template.cfg', 'r')
        string_cfg = cfg_file.read()        
        cfg_file.close()
        string_cfg = string_cfg.replace('<CLASS__NUM>', str(self.class_num))
        string_cfg = string_cfg.replace('<FILTERS__NUM>', str(5*(self.class_num+5)))
        new_file = open('./cfg/tiny-yolo-voc-new.cfg', 'w')
        new_file.writelines(string_cfg)
        new_file.close()
        pass

    def zipFiles(self):
        origin_folder = os.path.abspath('./TranningData')

        fantasy_zip = zipfile.ZipFile(origin_folder + '\\trainningData.zip', 'w')

        for file in os.listdir('./ckpt'):

            fantasy_zip.write(os.path.abspath('./ckpt/') + '\\' + file, './ckpt/' + file, compress_type = zipfile.ZIP_DEFLATED)
        fantasy_zip.write(os.path.abspath(DARKFLOW_MODEL), DARKFLOW_MODEL, compress_type = zipfile.ZIP_DEFLATED)
        fantasy_zip.close()

    def uploadTrainningFiles(self):
        with open(TRAINNINGDATA_PATH, 'rb') as f:
            bytes = f.read()
            encoded = base64.b64encode(bytes)
        base64file = str(encoded)
        params = {   
            'row': {
                'NRORG':NRORG,
                'tranningData': [
                    {
                        'name':'TranningBase.zip',
                        'b64File': 'data:aplication/zip;base64,' + base64file[2:],
                        'type':'aplication/zip',
                        'size': int(1000)
                    }
                ]
            }
        }
        str_json = json.dumps(params)
        requests.post(str(BASE_API_TRAIN) + 'uploadbase', json = str_json)


   

