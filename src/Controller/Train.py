import threading
from darkflow.net.build import TFNet
from src.Controller.FileController import FileController
from decouple import config

IMAGE_PATH = config('IMAGE_PATH')
ANNOTATION_PATH = config('ANNOTATION_PATH')
DATASET_FOLDER = config('DATASET_FOLDER')

class Train:
    def __init__(self, options):
        self.progressing = False
        self.tfnet = None
        self.options = options     
        self.fileControle = FileController(self)
        self.traningStats = {
            'description':str(),
            'status':str(),
            'step':int()
        }

    def startTrain(self):        
        self.tfnet = TFNet(self.options, self)
        self.tfnet.train()

    def setStatus(self, status):
        self.traningStats['status'] = status

    def flow(self):
        try:            
            self.progressing = True
            self.traningStats['step'] = 1
            self.traningStats['description'] = 'Recuperando URL do dataset'
            self.fileControle.getDatasetUrl()

            self.traningStats['step'] = 2
            self.traningStats['description'] = 'Baixando arquivos'
            self.fileControle.downloadFiles()
            
            self.traningStats['step'] = 3
            self.traningStats['description'] = 'Editando as anotações'
            self.fileControle.editAnnotation()
            
            self.traningStats['step'] = 4
            self.traningStats['description'] = 'Preparando dataset'
            self.fileControle.prepareDataSet()

            self.traningStats['step'] = 5
            self.traningStats['description'] = 'Listando classes'
            self.fileControle.generateLabelsFile()

            self.traningStats['step'] = 6
            self.traningStats['description'] = 'Gerando arquivo de configuração'
            self.fileControle.generateCfgFile()

            self.traningStats['step'] = 7
            self.traningStats['description'] = 'Treinamento do DataSet'
            self.setStatus('Iniciando treinamento')
            self.startTrain()
            self.traningStats['description'] = 'Treinamento finalizado'
            self.setStatus('Treinamento de dataset concluido')

            self.traningStats['step'] = 9
            self.traningStats['description'] = 'Criando Trainning Data'
            self.setStatus('Em andamento')
            self.fileControle.zipFiles()

            self.traningStats['step'] = int(10)
            self.traningStats['description'] = 'Subindo Tainning Data para a nuvem'
            self.setStatus('Em andamento')
            self.fileControle.uploadTrainningFiles()
            self.setStatus('Concluído')
            
            self.traningStats['step'] = int(11)
            self.traningStats['description'] = 'Rotina concluida'
            self.setStatus('Concluído')

            self.progressing = False

        except Exception as e:
            self.progressing = False
            self.traningStats['description'] = str(e)
            self.traningStats['status'] = str(False)
    
    def start(self):    
        t1 = threading.Thread(target = self.flow, daemon=True )
        t1.start()