import threading
import os
import base64
import traceback
import json

import cv2
import numpy as np
from flask import Flask, render_template, Response, request, make_response, current_app, jsonify
from werkzeug.utils import secure_filename
from botocore.config import Config
from decouple import config
from datetime import datetime, timedelta
from functools import update_wrapper

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = config('UPLOAD_FOLDER')
HTTP_HOST = config('HTTP_HOST')
HTTP_PORT = config('HTTP_PORT')

train = None

class WebServer:

    def __init__(self,train_):
        global train
        train = train_
        pass


    def crossdomain(origin=None, methods=None, headers=None, max_age=21600,
                attach_to_all=True, automatic_options=True):
        '''Decorator function that allows crossdomain requests.
            Courtesy of
            https://blog.skyred.fi/articles/better-crossdomain-snippet-for-flask.html
        '''
        if methods is not None:
            methods = ', '.join(sorted(x.upper() for x in methods))
        # use str instead of basestring if using Python 3.x
        if headers is not None and not isinstance(headers, basestring):
            headers = ', '.join(x.upper() for x in headers)
        # use str instead of basestring if using Python 3.x
        if not isinstance(origin, str):
            origin = ', '.join(origin)
        if isinstance(max_age, timedelta):
            max_age = max_age.total_seconds()

        def get_methods():
            ''' Determines which methods are allowed
            '''
            if methods is not None:
                return methods

            options_resp = current_app.make_default_options_response()
            return options_resp.headers['allow']

        def decorator(f):
            '''The decorator function
            '''
            def wrapped_function(*args, **kwargs):
                '''Caries out the actual cross domain code
                '''
                if automatic_options and request.method == 'OPTIONS':
                    resp = current_app.make_default_options_response()
                else:
                    resp = make_response(f(*args, **kwargs))
                if not attach_to_all and request.method != 'OPTIONS':
                    return resp

                h = resp.headers
                h['Access-Control-Allow-Origin'] = origin
                h['Access-Control-Allow-Methods'] = get_methods()
                h['Access-Control-Max-Age'] = str(max_age)
                h['Access-Control-Allow-Credentials'] = 'true'
                h['Access-Control-Allow-Headers'] = \
                    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
                if headers is not None:
                    h['Access-Control-Allow-Headers'] = headers
                return resp

            f.provide_automatic_options = False
            return update_wrapper(wrapped_function, f)
        return decorator
    

    def startServer(self):
        global HTTP_HOST
        global HTTP_PORT
        print(' [*] Starting WebServer.')
        app.run(host=HTTP_HOST, debug=False, port=HTTP_PORT)

   
    @app.route('/ping', methods=['GET','OPTIONS'])
    @crossdomain(origin='*')
    def ping():
        message = {
            'status':True,
            'message':'pong'}
        return jsonify(message)

    @app.route('/starttraning', methods=['GET','OPTIONS'])
    @crossdomain(origin='*')
    def start_traning():
        global train
        train.start()     
        return {
            'message':'Treinamento iniciado'
        }

    @app.route('/getstats', methods=['GET','OPTIONS'])
    @crossdomain(origin='*')
    def get_stats():
        global train
        try:
            return train.traningStats
        except Exception as e:
            return {
                'status':False,
                'description':e
            }


    def main(self):    
        t1 = threading.Thread(target=self.startServer, daemon=True )
        t1.start()